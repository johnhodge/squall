<?php
/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);					  
*/
$keyword=trim($_POST["searchsquall"]); 

function listFiles($dir,$keyword,&$array){ 

	$handle=opendir($dir); 

	while(false!==($file=readdir($handle))){ 

		if($file!="."&&$file!=".."){ 

			if(is_dir("$dir/$file")){ 

				listFiles("$dir/$file",$keyword,$array); 

			} 

			else{ 

				//read file 
				$isItHtml=stripos( $file,"html");
				if($isItHtml){ 
					$data=fread(fopen("$dir/$file","r"),filesize("$dir/$file")); 

					if($file!="search.php" && $file!="index.html"){ 
							if(preg_match("/$keyword/",$data)){ 

							$t = preg_match("/<h1>(.*)<\/h1>/siU", $data, $title_matches);
							if (!$t){ $title = "Untitled";} 
							else{
								 $title = preg_replace('/\s+/', ' ', $title_matches[1]);
								$title = trim($title);
							}
							$array[]="<p><a href=\"$dir/$file\" target=\"_blank\">$title</a></p>"; 
						}

					} 
				}

			} 

		} 

	} 

}

$array=array(); 



//echo/print search results 
?>
 <html lang="en">
 <head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 	<meta content="user-scalable=yes, width=device-width, initial-scale=1.0" name="viewport">
 	<meta content="yes" name="apple-mobile-web-app-capable">
 	<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
 <title>Squall - Search</title>

	<meta name="description" content="SQUALL Magazine: One of the UK’s foremost alternative media projects 1992-2006. The official archive of its journalism, design and photography">
	<meta name="keywords" content="direct action, protest, travellers, festivals, Stonehenge, Beanfield, squatting, Newbury, Fairmile, Claremont Rd, No M11 Link Rd, Criminal Justice Act 1994, Iraq Invasion, Prague, Seattle">
	  <meta content="user-scalable=yes, width=device-width, initial-scale=1.0" name="viewport">
	 <meta content="yes" name="apple-mobile-web-app-capable">
	 <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
	 <meta content="en_EN" property="og:locale">
	 <meta content="website" property="og:type">
	 <meta property="og:site_name" content="Squall Magazine">
	 <meta property="og:title" content="Squall Magazine - Necessity breeds ingenuity!">
	 <meta property="og:description" content="SQUALL Magazine: One of the UK’s foremost alternative media projects 1992-2006. The official archive of its journalism, design and photography">
	 <meta property="og:url" content="http://squallmagazine.com">
	 <meta property="og:image" content="http://squallmagazine.com/images/squall_facebook.jpg">
	 <meta name="twitter:card" content="summary_large_image">
	 <meta name="twitter:title" content="Squall Magazine - Necessity breeds ingenuity!">
	 <meta name="twitter:description" content="SQUALL Magazine: One of the UK’s foremost alternative media projects 1992-2006. The official archive of its journalism, design and photography">
	 <meta name="twitter:url" content="http://squallmagazine.com">
		<meta name="twitter:image:src" content="http://squallmagazine.com/images/squall_twitter.jpg">
	 <meta name="twitter:site" content="@squallmagazine">
	 <link href="http://squallmagazine.com" rel="canonical">
		<link href="http://squallmagazine.com/images/apple-touch-icon.png" rel="apple-touch-icon">
	 <link href="http://squallmagazine.com/images/apple-touch-icon-precomposed.png" rel="apple-touch-icon-precomposed">
 <link href="../css/squall.css" rel="stylesheet" type="text/css">
 <script type="text/javascript" src="../js/squall.js"></script>
 </head>
 <body class="f">
 <div class="wrapper">
 <header class="header">
 <div class="logo"><img src="../images/Squall-Logo-1-outline.png"/></div>
 <div class="necessity">Necessity Still Breeds Ingenuity - Archive&nbsp;of&nbsp;SQUALL&nbsp;MAGAZINE&nbsp;1992-2006</div>
 <div class="search-container"><form action="../search/search.php" method="post"><input type="text" id="searchsquall" name="searchsquall" placeholder="Stop and Search us"><button type="submit" name="search-button" id="search-button"></button></form></div>
 <div class="menu-button"></div>
 </header>
 
<nav class="nav">
<div class="search-container"><form action="../search.php" method="post"><input type="text" id="searchsquall" name="searchsquall" placeholder="Stop and Search us"><button type="submit" name="search-button" id="search-button"></button></form></div><p><a href="../index.html">About Squall</a></p>
	<p><!--<!--<a href="index.html#">-->Articles In Category<!--</a>--></p>
<ul><li style= "margin-left: 7px;"><a href="../f/index.html">Features</a><br><span id="notes">In-depth political analysis, interviews and investigative reports</span></li>
	<li style= "margin-left: 7px;"><a href="../fc/index.html">Frontline Communiques</a><br><span id="notes">Unedited dispatches from an action, event or incident</span></li>
	<li style= "margin-left: 7px;"><a href="../uu/index.html">Underground Updates</a><br><span id="notes">Reports, updates and bulletins</span></li>
	<li style= "margin-left: 7px;"><a href="../sii/index.html">The State It's In</a><br><span id="notes">Editorial and commentary from Squall central</span></li>
	<li style= "margin-left: 7px;"><a href="../r/index.html">Resources</a><br><span id="notes">Reference and ancillary articles</span></li>
</ul> 
	<p><a href="../back-issues.html">Print Output 1992-2001</a><br><span id="notes">Squall Magazine print output 1992-2001 with downloadable PDFs</span></p>
	<p><a href="index.html#">Articles 1998-2005 By Year</a><br><span id="notes">Online content ordered chronologically [under construction]</span></p>
	<p><a href="index.html#">Photos</a><br><span id="notes">Watch this space...</span></p>
	<p><a href="../r/links.html">Links</a></p>
	<p><a href="mailto:info@squallmagazine.com">Contact Squall</a></p>
	<p class="illuminations"><a href="#" class="lights on">Look on the Bright Side</a><a href="#" class="lights off">Over to The Dark Side</a></p>
	<p>&nbsp;</p>
	<div class="twit l">
		<a class="twitter-timeline" data-height="500" href="https://twitter.com/SQUALLMagazine?ref_src=twsrc%5Etfw">Tweets by SQUALLMagazine</a>
	</div>
	<div class="twit d">
		<a class="twitter-timeline d" data-height="500" data-theme="dark" data-link-color="#FAB81E" href="https://twitter.com/SQUALLMagazine?ref_src=twsrc%5Etfw">Tweets by SQUALLMagazine</a>
	</div>
	<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 
 <div class="socials">
		 <a href="http://twitter.com/squallmagazine" target="_blank" class="twitter-link"><img src="../images/twitter.png" alt="Link to @squallmagazine on Twitter"/></a>
		 <a href="http://facebook.com/squallmagazine" target="_blank" class="facebook-link"><img src="../images/facebook.png" alt="Link to squallmagazine on Facebook"/></a>
	 </div>
  </nav>
 
 <article class="article">
 <h1>What have we got here?</h1>
 <?php
	if($keyword==""){ 
		echo"<p>You forgot to mention what you were searching for! Is this some kind of set up?!</p>"; 
	} else{
		listFiles("..",$keyword,$array); 
		if(count($array) === 0){
			echo"<p>We got nothing!</p>";
		}
		else{
			foreach($array as $value){ 
				echo $value; 
			}  
		}	
	}
	
?>
   </article>
</div>
</body>
</html>
