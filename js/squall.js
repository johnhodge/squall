util = {
	getCookie: function (key) {
		var cookies = document.cookie.split('; ');
		for (var i = 0, l = cookies.length; i < l; i++) {
			var parts = cookies[i].split('=');
			if (parts.shift() === key) {
				return parts.join('=');
			}
		}
	},
	setCookie: function (c_name, value, exdays) {
        var exdate = new Date();
        exdate.setDate(exdate.getDate() + exdays);
        var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
        document.cookie = c_name + "=" + c_value;
    }
}
 
	
var goSquall = function(){
	if (document.body.classList){
		if(typeof util.getCookie('sqLights') !== "undefined"){
			if(util.getCookie('sqLights') === "dark"){
				document.body.classList.add('dark');
			}
		}
		if(document.querySelectorAll('.lights').length > 0){
			document.querySelectorAll('.lights').forEach(function(el){
				el.addEventListener('click',function(e){
					e.preventDefault();
					document.querySelector('body').classList.toggle('dark');
					if(document.querySelector('body').classList.contains('dark')){
						util.setCookie('sqLights','dark',365);
					}
					else{
						util.setCookie('sqLights','light',365);
					}
				});
			});
		}
	} else {
		document.querySelector('.illuminations').style.display = "none";
	}
	if(document.querySelectorAll('.menu-button').length > 0){
		document.querySelector('.menu-button').addEventListener('click', function (e) {
			document.querySelector('.menu-button').classList.toggle('shown');
			document.querySelector('.nav').classList.toggle('shown');
		});
	}
}

 var detectIE = function () {
	var ua = window.navigator.userAgent;

	var trident = ua.indexOf('Trident/');
	if (trident > 0) {
		// IE 11 => return version number
		var rv = ua.indexOf('rv:');
		return "IE" + parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
	}

	var edge = ua.indexOf('Edge/');
	if (edge > 0) {
		// Edge (IE 12+) => return version number
		return "Edge" + parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
	}

	// other browser
	return false;
}
var isIEVersion = detectIE();

if (isIEVersion && isIEVersion.indexOf('IE') > -1) {
	//we've checked and this is IE and not Edge
	//polyfill querySelectorAll forEach loops:
	if (typeof NodeList !== "undefined" && NodeList.prototype && !NodeList.prototype.forEach) {
		NodeList.prototype.forEach = Array.prototype.forEach;
	}
}
var stateCheck = setInterval(function(){
  if (document.readyState === 'complete') {
		clearInterval(stateCheck);
		goSquall();
  }
}, 100);
